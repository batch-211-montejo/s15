console.log ("Hello World");

//Javascript renders webpages in an interactive and dynamic fashion. Meaning it enables us to create dynamically updating content, control multimedia and animate images.

//Syntax, Statement and Comment

//Statement;
//Statements in programming are instruction that we tell the computer to perform
//JS Statements usually end with a semicolon (;)
// Semicolons are not required in KS, but we will use it to help us train to locate where a statement ends.

//Syntax in programming, it is the set of rules that describes how statements must be constructed

//Comments are parts of the code that gets ignored by the language
//Comments are meant to describe the written code


/*
	There are teo types of comments
	1. The signle-line comment (ctrl + /)
	**denoted by two slashes

	2. The multi-line comment (ctrl + shift + /)
	**denoted by a slahs and asterisk

*/


//Variables
//It is used to contain data
//This makes it easier to associate indormation stored in our devices to actual "names" about information

//Declaring variables
// Declaring avriables - tells our devices that a variable name is created and is ready to store data

let myVariable;
console.log(myVariable); 

// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was not defined

//Syntax
	// let/const variableName;

// console.log() is useful fo printing values of variables or certain results of code into Google Chrome browser's console
// constant use of this throught developing an application will save us time and builds good habits in always checking for the output of our code.

let hello;
console.log(hello); 

// Variables must be declared first before they are used
// Using variables before they are declared will return an error

/*
	Guides in wring variables
		1. Use the 'let' keyword followed by the variable name of your choosing and use the assignement operator (=) to assign a value
		2. Variable names should start with a lowercase character, use camelCase
		3. For constant variables, use the 'const' keyworkd
		4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion
*/

// Declare and Initialize variables
	// intialiszin variables - the instance when a vriable is given its initial or starting value

	// Sytantax
		// let/const variableName = value;

		let productName = "desktop computer";
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

		const interest = 3.539;
		// const pi = 3.1416;

		// Reassigning variable values
		// reassingin a variable means changing its initial or previous value into another value
		// Syntax 
			// variableName = newValue;
		productName = 'laptop'
		console.log(productName);


		// console.log(interest);
		// console.log(pi)

		// let variable cannot be redecalred within its scope so this will work
		// let friend = 'Kate'
		// friend = 'Nej'
		// console.log(friend);

		// let friend = "Kate";
		// let friend = "Nej"; //error
		// console.log(friend);
		// error: indentifier 'friend' has already been decalred

		// interest = 4.489; //error

// Reassigning variables vc initializing variables
// decalre a variable first

	let supplier;
	supplier = "John Smith Tradings";
	// initialization is done after the variable has been declared
	// This is considered as initialization because it is the first time that a value has been assigned to a variable
	console.log(supplier);

	// considered as reassignment because its initial value was already declared
	supplier = "Zuitt Store";
	console.log(supplier);

	// we cannot declare a const variable without initialization
	// const pi;
	// pi = 3.1416;
	// console.log(pi); //error

	// var 
	// var is also used in declaring a variable
	// let/const they were used introduced as new feature in E26 (2015)

	// a = 5;
	// console.log(5);
	// var a;

	// hoisting is JS's default behavior of moving declarations to the top
	// In terms of variables and constants, keywork var is hoisted and let and const does not allow hoisting


	// a = 5;
	// console.log(a);
	// let a;

// let/const local/global scope
	// scope essentially means where these variables are available for use
	// let and const are blocked-scoped
	// block is a chunk of code by {}. A block live in curly braces. Anything within curly braces is a block
	// so a variable declared in a block with let is only avaialable for usr within that block

	// let outerVariable = "Hello";

	// {
	// 	let innerVariable = "Hello again";
	// }

	// console.log(outerVariable);
	// console.log(innerVariable); //innerVariable is not defined

	// Multiple variable declarations
	// multiple variables may be declared in one line
	// though it is quicker to do without having to retype the "let" keyword, it is still best practive to use multiple "let/const" keywords when declaring variables
	// using multiple keywords makes codes easier to read abd determine what kind of variable has been created;

	// let productCode = 'DC017', productBrand = "Dell";
	let productCode = 'DC017';
	const productBrand = "Dell";
	console.log(productCode, productBrand); //DC017, Dell

	// Using a variable with a reserved keywork
	// const let = "hello";
	// console.log(let); //error

	// Date Types

	// Strings 
	// Strings are a seties of characters that create a word, phase sentence or anything related to creating text
	// Strings in JS cen be written using either ('') or double ("") quote
	// In other programming languages, only the double quotes can be used for creating strings

	let country = "Philippines";
	let province = 'Metro Manila';

	// Concatenating strings
	// Multiple string values can be conbined to create a single string using the "+" symbol

	let fullAddress = province + ', ' + country;
	console.log(fullAddress);

	let greeting = "I live in the" + country;
	console.log(greeting);

	//The escape character (\) in strings in combination with other characters can produce differenet effects
	// "\n" refers to creating a new line in between text

	let mailAddress = 'Metro Manila\n\nPhilippines';
	console.log(mailAddress);
	// Metro manila

	// Philippines

	// Using teh doible quotes along with single quotes can allow as include single quotes in texts without using the escape character

	let message = "John's employees went home early";
	console.log(message);
	message = 'John\'s employees went home early';
	console.log(message);

	// Numbers

	// Integers/whole Numbers

	let headcount = 26;
	console.log(headcount);

	// Decimals numbers/fractions
	let grade = 98.7;
	console.log(grade); //98.7

	// Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

	// combining tect and strings
	console.log("John's grade last quarter is " + grade);
	// John's grade last quarter is 98.7

	// Boolean
	// Boolean values are normally used to restore values relating to the state of certain things
	// This will be useful in further discussions about creating logic to makae our applciation respond to certain situations/scenarios

	let isMarried = false;
	let inGoodConduct = true;
	console.log("is Married: " + isMarried);
	console.log("inGoodConduct: " + inGoodConduct);

	// Arrays
	// Arrays are a special kind of data type taht is used to store multipel values
	// Arrays can store different data type but is normally used to store similar data tyoes

	// similar data types
	// syntax
	// let/const arrayName = [elementA, elementB, elementC,...]
		// let 

		let grades = [98.7, 92.1, 90.2, 94.6];
		console.log(grades);

		// different data types
		// storing different data types inside an array is not recommended becuase it will nto make send in the context of progarmming

		let detailes = ["John", "Smith", 32, true];
		console.log(detailes);

		// Objects
		// Objects are another special king of data types that's used to mimic real world objects.items
		// They're used to create complex data what containes pieces of information that are relevant to each other
		// Each individial piece of information is called a property of the object

		// Syntax
			// let/const objectName={
				//propertyA: value,
				//propertyB: value
			//}

			let person = {
				fullName: 'Edward Scissorhands',
				age: 25,
				isMarried: false,
				contact: ["+639121234567", "8123 4567"],
				address: {
					houseNumber: '345',
					city: 'Manila'
				}
			}

			console.log(person);
			let myGrades = {
				firstGrading: 98.7,
				secondGrading: 92.1,
				thirdGrading: 90.2,
				fourthGrading: 94.6
			}
			console.log(myGrades);

			// type of operator
			console.log (typeof myGrades); //object
			console.log(typeof grades); //object
			// note: array is a special type of object with methods and functions to manipulate it
			// we will discuss these methods in later session (s22-array manipulation)

			const anime = ['OP', 'OPM', 'AOT', 'BNHA']
			anime[0] = 'JJK';

			console.log(anime);

			// Null
			// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified
			let spouse = null;
			console.log(spouse);
			// null is also considered a data type of its own comapred to 0 which is a numbe and signle quotes which are a data type of a string

			let myNumber = 0 //0
			let myString = '' //
			console.log(myNumber);
			console.log(myString);

			//Undefined 
			// represemts the state of a variable that has been declared but w/o an assigned value

			let fullName;
			console.log(fullName); //Undefined

			// once clear difference between undefined and null is that for undefined, a vairiable was created but was not provided a value